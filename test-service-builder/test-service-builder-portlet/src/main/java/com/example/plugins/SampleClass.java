package com.example.plugins;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.example.plugins.model.Foo;
import com.example.plugins.service.FooLocalServiceUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.messaging.Message;
import com.liferay.portal.kernel.messaging.MessageListener;
import com.liferay.portal.kernel.messaging.MessageListenerException;
import com.liferay.portal.kernel.repository.model.Folder;
import com.liferay.portal.kernel.util.MimeTypesUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Company;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.User;
import com.liferay.portal.security.auth.PrincipalThreadLocal;
import com.liferay.portal.security.permission.PermissionChecker;
import com.liferay.portal.security.permission.PermissionCheckerFactoryUtil;
import com.liferay.portal.security.permission.PermissionThreadLocal;
import com.liferay.portal.service.CompanyLocalServiceUtil;
import com.liferay.portal.service.RoleLocalServiceUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextFactory;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portlet.documentlibrary.model.DLFileEntry;
import com.liferay.portlet.documentlibrary.model.DLFolderConstants;
import com.liferay.portlet.documentlibrary.service.DLAppLocalServiceUtil;
import com.liferay.portlet.documentlibrary.service.DLAppServiceUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

public class SampleClass extends MVCPortlet implements MessageListener {

	private static Log _log = LogFactoryUtil.getLog(SampleClass.class);

	private String LRTempLocation = "D:/liferay-portal-6.2-ce-ga6/temp/";
	private String DLFolderLocation = "folder-1";

	// Repository ID of the document library, figured is out by the output from
	// _log.debug("repositoryId: " + repositoryId);
	// in
	// fileUploadByApp(folderName, themeDisplay, renderRequest).
	// I found the entry in 'ddmcontent' table of LR's default DB
	private long siteARepoId = 21303;
	// Found this in 'company' table of LR's default DB
	private long LRCompanyId = 20154;
	private String sAdminRole = "Administrator";

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse)
			throws PortletException, IOException {
		_log.debug(">> render()");

		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
		fileUploadByApp(DLFolderLocation, themeDisplay, renderRequest);

		_log.debug("<< render()");
		super.render(renderRequest, renderResponse);
	}

	@Override
	public void receive(Message message) throws MessageListenerException {
		_log.debug(">> receive()");

		ServiceContext serviceContext = new ServiceContext();
		serviceContext.setScopeGroupId(siteARepoId);
		fileUploadByApp(DLFolderLocation, serviceContext);

		_log.debug("<< receive()");
	}

	public void fileUploadByApp(String folderName, ServiceContext serviceContext) {
		_log.debug(">> fileUploadByApp(folderName, serviceContext)");

		try {
			File file = new File(LRTempLocation + "excel_" + getDateTimeBasedFilename() + ".xlsx");

			createExcel(file);

			long repositoryId = serviceContext.getScopeGroupId();
			String mimeType = MimeTypesUtil.getContentType(file);
			String title = file.getName();
			String description = "This file is added via programatically";
			String changeLog = "hi";
			Long parentFolderId = DLFolderConstants.DEFAULT_PARENT_FOLDER_ID;

			_log.debug("repositoryId: " + repositoryId);
			_log.debug("fileName: " + file.getName());
			_log.debug("mimeType: " + mimeType);
			_log.debug("title: " + title);
			_log.debug("description: " + description);
			_log.debug("changeLog: " + changeLog);
			_log.debug("fileLength: " + file.length());
			_log.debug("serviceContext: " + serviceContext);

			Folder folder = DLAppLocalServiceUtil.getFolder(repositoryId, parentFolderId, folderName);

			_log.debug("folderId: " + folder.getFolderId());

			InputStream is = new FileInputStream(file);

			_log.debug("inputStream: " + is);

			// Initializing PermissionChecker, without the below the exception
			// occurs
			// Caused by: com.liferay.portal.security.auth.PrincipalException:
			// PermissionChecker not initialized
			// Source:
			// http://www.open.gr/blog/2014/05/permissionchecker-not-initialised-scheduler-job
			Company companyqq = CompanyLocalServiceUtil.getCompanyById(LRCompanyId);
			Role adminRole = RoleLocalServiceUtil.getRole(companyqq.getCompanyId(), sAdminRole);
			List<User> adminUsers = UserLocalServiceUtil.getRoleUsers(adminRole.getRoleId());

			PrincipalThreadLocal.setName(adminUsers.get(0).getUserId());
			PermissionChecker permissionChecker = PermissionCheckerFactoryUtil.create(adminUsers.get(0), true);
			PermissionThreadLocal.setPermissionChecker(permissionChecker);

			// Adding the file entry to the Document Library
			DLAppServiceUtil.addFileEntry(repositoryId, folder.getFolderId(), file.getName(), mimeType, title,
					description, changeLog, is, file.length(), serviceContext);

		} catch (Exception e) {
			System.out.println("Exception");
			e.printStackTrace();
		}

		_log.debug("<< fileUploadByApp(folderName, serviceContext)");
	}

	// Source:
	// http://liferayiseasy.blogspot.ca/2015/07/folder-and-file-upload-programmatically.html
	public void fileUploadByApp(String folderName, ThemeDisplay themeDisplay, RenderRequest renderRequest) {
		_log.debug(">> fileUploadByApp(folderName, themeDisplay, renderRequest)");

		try {
			File file = new File(LRTempLocation + "excel_" + getDateTimeBasedFilename() + ".xlsx");

			createExcel(file);

			long repositoryId = themeDisplay.getScopeGroupId();
			String mimeType = MimeTypesUtil.getContentType(file);
			String title = file.getName();
			String description = "This file is added via programatically";
			String changeLog = "hi";
			Long parentFolderId = DLFolderConstants.DEFAULT_PARENT_FOLDER_ID;

			Folder folder = DLAppServiceUtil.getFolder(themeDisplay.getScopeGroupId(), parentFolderId, folderName);
			ServiceContext serviceContext = ServiceContextFactory.getInstance(DLFileEntry.class.getName(),
					renderRequest);
			InputStream is = new FileInputStream(file);

			_log.debug("repositoryId: " + repositoryId);
			_log.debug("folderId: " + folder.getFolderId());
			_log.debug("fileName: " + file.getName());
			_log.debug("mimeType: " + mimeType);
			_log.debug("title: " + title);
			_log.debug("description: " + description);
			_log.debug("changeLog: " + changeLog);
			_log.debug("inputStream: " + is);
			_log.debug("fileLength: " + file.length());
			_log.debug("serviceContext: " + serviceContext);
			DLAppServiceUtil.addFileEntry(repositoryId, folder.getFolderId(), file.getName(), mimeType, title,
					description, changeLog, is, file.length(), serviceContext);

		} catch (Exception e) {
			System.out.println("Exception");
			e.printStackTrace();
		}

		_log.debug("<< fileUploadByApp(folderName, themeDisplay, renderRequest)");
	}

	public void createExcel(File file) {
		_log.debug(">> createExcel()");

		try {
			FileOutputStream out = new FileOutputStream(file);
			XSSFWorkbook workbook = new XSSFWorkbook();
			XSSFSheet spreadsheet = workbook.createSheet("foo");

			XSSFRow row0 = spreadsheet.createRow(0);
			row0.createCell(0).setCellValue("fooId");
			row0.createCell(1).setCellValue("groupId");
			row0.createCell(2).setCellValue("companyId");
			row0.createCell(3).setCellValue("userId");
			row0.createCell(4).setCellValue("userName");
			row0.createCell(5).setCellValue("createDate");
			row0.createCell(6).setCellValue("modifiedDate");
			row0.createCell(7).setCellValue("field1");
			row0.createCell(8).setCellValue("field2");
			row0.createCell(9).setCellValue("field3");
			row0.createCell(10).setCellValue("field4");
			row0.createCell(11).setCellValue("field5");
			row0.createCell(12).setCellValue("uuid_");

			List<Foo> lFoo = FooLocalServiceUtil.getFoos(-1, -1);
			int rowCount = 1;
			XSSFRow row = null;
			XSSFCell cell = null;
			for (Foo foo : lFoo) {
				row = spreadsheet.createRow(rowCount);

				row = spreadsheet.createRow(rowCount);
				cell = row.createCell(0);
				cell.setCellValue(foo.getFooId());
				cell = row.createCell(1);
				cell.setCellValue(foo.getGroupId());
				cell = row.createCell(2);
				cell.setCellValue(foo.getCompanyId());
				cell = row.createCell(3);
				cell.setCellValue(foo.getUserId());
				cell = row.createCell(4);
				cell.setCellValue(foo.getUserName());
				cell = row.createCell(5);
				cell.setCellValue(foo.getCreateDate());
				cell = row.createCell(6);
				cell.setCellValue(foo.getModifiedDate());
				cell = row.createCell(7);
				cell.setCellValue(foo.getField1());
				cell = row.createCell(8);
				cell.setCellValue(foo.getField2());
				cell = row.createCell(9);
				cell.setCellValue(foo.getField3());
				cell = row.createCell(10);
				cell.setCellValue(foo.getField4());
				cell = row.createCell(11);
				cell.setCellValue(foo.getField5());
				cell = row.createCell(12);
				cell.setCellValue(foo.getUserUuid());

				rowCount++;
			}

			workbook.write(out);
			workbook.close();
			out.close();
		} catch (Exception e) {
			System.out.println("Exception");
			e.printStackTrace();
		}

		_log.debug("<< createExcel()");
	}

	public String getDateTimeBasedFilename() {
		_log.debug(">> getDateTimeBasedFilename()");

		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");

		_log.debug("<< getDateTimeBasedFilename()");
		return dateFormat.format(date);
	}

	public void dump() throws Exception {
		File file = new File(LRTempLocation + "sample_" + getDateTimeBasedFilename() + ".txt");

		Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "utf-8"));
		writer.write("Something");
		writer.close();
	}

}
