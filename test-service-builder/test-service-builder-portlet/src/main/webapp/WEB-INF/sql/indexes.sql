create index IX_8F599731 on testservicebuilder_Foo (field2);
create index IX_A668F9CF on testservicebuilder_Foo (uuid_);
create index IX_A89022F9 on testservicebuilder_Foo (uuid_, companyId);
create unique index IX_A78713BB on testservicebuilder_Foo (uuid_, groupId);