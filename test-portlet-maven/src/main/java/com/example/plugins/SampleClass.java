package com.example.plugins;

import java.io.IOException;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

public class SampleClass extends MVCPortlet {

	private static Log _log = LogFactoryUtil.getLog(SampleClass.class);

	@Override
	public void render(RenderRequest request, RenderResponse response) throws PortletException, IOException {
		_log.debug(">> render()");

		_log.debug("<< render()");
		super.render(request, response);
	}

	@Override
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse)
			throws IOException, PortletException {
		_log.debug(">> serveResource()");

		String form = ParamUtil.getString(resourceRequest, "form");
		String report = ParamUtil.getString(resourceRequest, "report");

		_log.debug("form: " + form);
		_log.debug("report: " + report);

		_log.debug("<< serveResource()");
		super.serveResource(resourceRequest, resourceResponse);
	}

}
