<%--
/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
--%>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="theme"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>

<theme:defineObjects />
<portlet:defineObjects />
<portlet:resourceURL var="resourceURL" />
<script type="text/javascript">
function reportForm(formNum, reportNum) {
	AUI().use('aui-io-request', function(A) {
		A.io.request('<%=resourceURL.toString()%>', {
			method : 'post',
			data : {
				form : formNum,
				report : reportNum
			}
		});
	});
}
</script>

<aui:container>
	<aui:row>
		<aui:column>
			<aui:row>Form 1</aui:row>
			<aui:row>
				<aui:form>
					<aui:button value="Report 1" onClick="reportForm(1, 1)" />
					<aui:button value="Report 2" onClick="reportForm(1, 2)" />
					<aui:button value="Report 3" onClick="reportForm(1, 3)" />
					<aui:button value="Report 4" onClick="reportForm(1, 4)" />
				</aui:form>
			</aui:row>
		</aui:column>
		<aui:column>
			<aui:row>Form 2</aui:row>
			<aui:row>
				<aui:form>
					<aui:button value="Report 1" onClick="reportForm(2, 1)" />
					<aui:button value="Report 2" onClick="reportForm(2, 2)" />
					<aui:button value="Report 3" onClick="reportForm(2, 3)" />
					<aui:button value="Report 4" onClick="reportForm(2, 4)" />
				</aui:form>
			</aui:row>
		</aui:column>
	</aui:row>
</aui:container>